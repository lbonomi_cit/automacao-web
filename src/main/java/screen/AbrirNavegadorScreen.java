package screen;

import util.Utils;

public class AbrirNavegadorScreen extends Utils{

	public void acessarChrome() {
		webDriver = getWebdriverChrome();
		webDriver.manage().window().maximize();
		setWebDriver(webDriver);
	}
	
	public void acessarUrl(String url) {
		webDriver = getWebDriver();
		webDriver.get(url);
		setWebDriver(webDriver);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void fecharChrome() {
		webDriver.quit();
	}
}
