# language: pt
# encoding: utf-8

@tag
Funcionalidade: Abrir Navegador

@AcessarSite
	Cenario: Abrir o navegador chrome
		Dado eu abra o navegador chrome
		Quando insiro a url do site
		Entao fecho o chrome
