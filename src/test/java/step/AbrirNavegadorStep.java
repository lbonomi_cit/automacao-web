package step;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import screen.AbrirNavegadorScreen;

public class AbrirNavegadorStep extends AbrirNavegadorScreen{

	@Dado("eu abra o navegador chrome")
	public void test() {
		acessarChrome();
	}

	@Quando("insiro a url do site")
	public void insiroAUrlDoSite() {
		acessarUrl("https://www.google.com");
	}

	@Entao("fecho o chrome")
	public void fechoOChrome() {
		fecharChrome();
	}

}
